# 提案

## 指南

在向本文添加提案时，请先思考以下事项：

* 简要说明
* 指向项目存储库的 URL 链接
* 项目的简单和困难评级
* 预期结果
* 从事项目的先决条件（所需技能/首选）
* 您的姓名和电子邮件地址
* 如果需要提醒学生工作量，项目的预期规模（小时）。

添加提案时请保持以下模板:

```markdown
### 项目名称

导师：**你的名字** <你的邮箱>

项目仓库：<URL>

项目协议：<GPL/MIT/BSD>

项目难度：基础/进阶

技术领域：OS；Linux；RISC-V；C++；等等

#### 项目描述

> 项目描述应填写项目的
> （1）一些相关背景
> （2）已有的工作
> （3）存在的不足
> （4）希望改进的点
> （5）最终项目实现的目标

#### （可选）项目备注

> 一些额外的信息。

#### 项目产出要求

> 明确描述项目结项时需要达成的目标、结果、指标、产物，以列表方式提供。
>
> 1. xxx
> 2. xxx

#### 项目技术要求

> 以列表方式列出项目开发的前置技术要求。
>
> 1. 熟悉 C++
> 2. 熟悉 LLVM
> 3. 熟悉内核 xx 模块

```

## 提案列表

### PolyOS 基础设施改进

导师：常秉善 <changbingshan@iscas.ac.cn>

项目仓库：https://gitee.com/riscv-raios/polyos-site, https://gitee.com/riscv-raios/polyos-docs

项目协议：MIT

项目难度：基础

技术领域：Web；React；Container；

#### 项目描述

PolyOS 社区对社区的操作系统为用户提供了官网、下载、文档、流量监控等服务，目前主要为 PolyOS Mobile 提供了相关的托管服务。现计划为 PolyOS AIoT 提供服务，并对架构、技术栈、服务性能进行改进。目前相关服务主要使用 React、Docusaurus、Plausible、Nginx 等框架和服务。

#### 项目产出

1. 为 PolyOS AIoT 提供文档服务；

#### 项目技术要求

1. 熟悉 TypeScript；
2. 熟悉 React；
3. 了解 Linux、Docker；
4. 了解 Pandoc 等工具；

### 基于 Qt 的 Qemu 移动终端模拟器改进

导师：陈荣 <chenrong@iscas.ac.cn>

项目仓库：https://gitee.com/polyos-project/qemu

项目协议：GPLv2

项目难度：进阶

技术领域：OpenHarmony；Linux；C++；QEMU；

#### 项目描述

PolyOS Mobile 是基于 OpenHarmony for RISC-V 一款移动操作系统，目前已支持利用 Qemu 模拟器使用这款操作系统，当前项目使用的 Qemu 已经支持 SDL 和 GTK 开发的侧边栏功能，为了提供更好的体验，在此需要使用 Qt 重构 Qemu 的主窗口并开发其他 ui 部分。

#### 项目产出

1. 利用 Qt 开发 Qemu 界面，包括主窗口，侧边栏，设置窗口等。

#### 项目技术要求

1. 熟悉 C++；
2. 熟悉 Qt；
3. 熟悉 Qemu；

### 为PolyOS for QEMU增加ALSA音频框架支持

导师：全雨  <quanyu@iscas.ac.cn>

项目仓库：待补充

项目协议：

项目难度：进阶

技术领域：OpenHarmony；Linux；ALSA；QEMU；C++；

#### 项目描述

本项目旨在为 PolyOS 增加 ALSA (Advanced Linux Sound Architecture) 音频框架支持。通过此项目，我们将开发和调整必要的驱动和中间件，使得 PolyOS 能够在 QEMU for RISCV 模拟器上支持 ALSA 音频框架，并在模拟环境中启动音乐应用程序，播放音频文件。本项目将增强 PolyOS 在 QEMU for RISCV 上的多媒体性能。

#### 项目产出

1. 生成 QEMU for RISCV 的 PolyOS 操作系统镜像，该镜像应包含所有集成和优化 ALSA 音频框架，确保在 QEMU 环境中可以运行，音乐 app 能流畅播放音频文件
2. 提供详细的安装和配置指南，以及如何在 QEMU 模拟器中启动和测试音频功能的步骤

#### 项目技术要求

1. 熟悉嵌入式linux系统开发
2. 具备C/C++编程能力


### ArkCompiler ETS Runtime assembler 的 RISCV 支持

导师：常秉善 <changbingshan@iscas.ac.cn>

项目仓库：https://gitee.com/polyos-project/arkcompiler_ets_runtime.git

技术领域：编译器；RISC-V；Linux；C++；

#### 项目描述

本项目希望帮助 PolyOS 增强 RISCV 上的 ArkCompiler ETS Runtime。你将在此项目中完成所有 RISCV 基本指令集指令在 ArkCompiler assembler 中的翻译，并针对 RISCV 架构修复所有接口的参数使其不会破坏其他架构（aarch64、x86_64）。目前，已具有一些基础的实现，你需要检查并修复所有实现，并在其基础上完成剩余所有指令的实现。

#### 项目产出

1. 在 ArkCompiler assembler 实现所有 RISCV 基础指令集的指令，测试并输出用例的验证性报告或文档；
2. 接口、指令实现的标准文档；

#### 项目技术要求

1. 熟悉 RISCV 指令集；
2. 了解 aarch64 指令集，并理解其部分指令的编码约定；
